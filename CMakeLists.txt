cmake_minimum_required(VERSION 2.8 )
project(lab6)

find_package(OpenCV REQUIRED)

include_directories(include ${OpenCV_INCLUDE_DIRS})


add_executable(lab6 src/lab6.cpp include/ObjectTrackerTest.cpp include/ObjectTrackerTest.h)
target_link_libraries(lab6 ${OpenCV_LIBS})
